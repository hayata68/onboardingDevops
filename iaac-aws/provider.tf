# Define AWS as our provider
provider "aws" {
  profile = "lab"

  # access_key = "${var.aws_access_key}"
  # secret_key = "${var.aws_secret_key}"

  region  = "${var.aws_region}"
}
